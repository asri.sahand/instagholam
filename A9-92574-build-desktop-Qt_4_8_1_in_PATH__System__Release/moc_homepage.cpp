/****************************************************************************
** Meta object code from reading C++ file 'homepage.h'
**
** Created: Tue May 27 12:09:33 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../A9-92574/homepage.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'homepage.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_HomePage[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x08,
      35,    9,    9,    9, 0x08,
      62,    9,    9,    9, 0x08,
      90,    9,    9,    9, 0x08,
     122,    9,    9,    9, 0x08,
     150,    9,    9,    9, 0x08,
     177,    9,    9,    9, 0x08,
     202,    9,    9,    9, 0x08,
     231,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_HomePage[] = {
    "HomePage\0\0on_test_Button_clicked()\0"
    "on_logout_Button_clicked()\0"
    "on_profile_Button_clicked()\0"
    "on_uploadphoto_Button_clicked()\0"
    "on_confirm_Button_clicked()\0"
    "on_reject_Button_clicked()\0"
    "on_next_Button_clicked()\0"
    "on_previous_Button_clicked()\0"
    "on_like_Button_clicked()\0"
};

void HomePage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        HomePage *_t = static_cast<HomePage *>(_o);
        switch (_id) {
        case 0: _t->on_test_Button_clicked(); break;
        case 1: _t->on_logout_Button_clicked(); break;
        case 2: _t->on_profile_Button_clicked(); break;
        case 3: _t->on_uploadphoto_Button_clicked(); break;
        case 4: _t->on_confirm_Button_clicked(); break;
        case 5: _t->on_reject_Button_clicked(); break;
        case 6: _t->on_next_Button_clicked(); break;
        case 7: _t->on_previous_Button_clicked(); break;
        case 8: _t->on_like_Button_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData HomePage::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject HomePage::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_HomePage,
      qt_meta_data_HomePage, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &HomePage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *HomePage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *HomePage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_HomePage))
        return static_cast<void*>(const_cast< HomePage*>(this));
    return QDialog::qt_metacast(_clname);
}

int HomePage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
