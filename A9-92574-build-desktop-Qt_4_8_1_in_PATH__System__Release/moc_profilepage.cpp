/****************************************************************************
** Meta object code from reading C++ file 'profilepage.h'
**
** Created: Tue May 27 12:09:34 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../A9-92574/profilepage.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'profilepage.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ProfilePage[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x08,
      38,   12,   12,   12, 0x08,
      65,   12,   12,   12, 0x08,
      90,   12,   12,   12, 0x08,
     121,   12,   12,   12, 0x08,
     147,   12,   12,   12, 0x08,
     179,   12,   12,   12, 0x08,
     209,   12,   12,   12, 0x08,
     242,   12,   12,   12, 0x08,
     271,   12,   12,   12, 0x08,
     306,   12,   12,   12, 0x08,
     337,   12,   12,   12, 0x08,
     367,   12,   12,   12, 0x08,
     392,   12,   12,   12, 0x08,
     421,   12,   12,   12, 0x08,
     445,   12,   12,   12, 0x08,
     487,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ProfilePage[] = {
    "ProfilePage\0\0on_home_Button_clicked()\0"
    "on_logout_Button_clicked()\0"
    "on_test_Button_clicked()\0"
    "on_changepass_Button_clicked()\0"
    "on_Enter_Button_clicked()\0"
    "on_uploadphoto_Button_clicked()\0"
    "on_addfriend_Button_clicked()\0"
    "on_enter_friend_Button_clicked()\0"
    "on_unfriend_Button_clicked()\0"
    "on_enter_unfriend_Button_clicked()\0"
    "on_showfriendsButton_clicked()\0"
    "on_showusers_Button_clicked()\0"
    "on_next_Button_clicked()\0"
    "on_Previous_Button_clicked()\0"
    "on_pushButton_clicked()\0"
    "on_friends_profile_Enter_Button_clicked()\0"
    "on_like_Button_clicked()\0"
};

void ProfilePage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ProfilePage *_t = static_cast<ProfilePage *>(_o);
        switch (_id) {
        case 0: _t->on_home_Button_clicked(); break;
        case 1: _t->on_logout_Button_clicked(); break;
        case 2: _t->on_test_Button_clicked(); break;
        case 3: _t->on_changepass_Button_clicked(); break;
        case 4: _t->on_Enter_Button_clicked(); break;
        case 5: _t->on_uploadphoto_Button_clicked(); break;
        case 6: _t->on_addfriend_Button_clicked(); break;
        case 7: _t->on_enter_friend_Button_clicked(); break;
        case 8: _t->on_unfriend_Button_clicked(); break;
        case 9: _t->on_enter_unfriend_Button_clicked(); break;
        case 10: _t->on_showfriendsButton_clicked(); break;
        case 11: _t->on_showusers_Button_clicked(); break;
        case 12: _t->on_next_Button_clicked(); break;
        case 13: _t->on_Previous_Button_clicked(); break;
        case 14: _t->on_pushButton_clicked(); break;
        case 15: _t->on_friends_profile_Enter_Button_clicked(); break;
        case 16: _t->on_like_Button_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ProfilePage::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ProfilePage::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ProfilePage,
      qt_meta_data_ProfilePage, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ProfilePage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ProfilePage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ProfilePage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ProfilePage))
        return static_cast<void*>(const_cast< ProfilePage*>(this));
    return QDialog::qt_metacast(_clname);
}

int ProfilePage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
