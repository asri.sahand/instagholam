/****************************************************************************
** Meta object code from reading C++ file 'uploadpic.h'
**
** Created: Tue May 27 12:09:35 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../A9-92574/uploadpic.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'uploadpic.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_uploadepic[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      39,   11,   11,   11, 0x08,
      64,   11,   11,   11, 0x08,
      89,   11,   11,   11, 0x08,
     117,   11,   11,   11, 0x08,
     147,   11,   11,   11, 0x08,
     172,   11,   11,   11, 0x08,
     199,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_uploadepic[] = {
    "uploadepic\0\0on_Logout_Button_clicked()\0"
    "on_test_Button_clicked()\0"
    "on_home_Button_clicked()\0"
    "on_profile_Button_clicked()\0"
    "on_showimage_Button_clicked()\0"
    "on_open_Button_clicked()\0"
    "on_cancel_Button_clicked()\0"
    "on_OK_Button_clicked()\0"
};

void uploadepic::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        uploadepic *_t = static_cast<uploadepic *>(_o);
        switch (_id) {
        case 0: _t->on_Logout_Button_clicked(); break;
        case 1: _t->on_test_Button_clicked(); break;
        case 2: _t->on_home_Button_clicked(); break;
        case 3: _t->on_profile_Button_clicked(); break;
        case 4: _t->on_showimage_Button_clicked(); break;
        case 5: _t->on_open_Button_clicked(); break;
        case 6: _t->on_cancel_Button_clicked(); break;
        case 7: _t->on_OK_Button_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData uploadepic::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject uploadepic::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_uploadepic,
      qt_meta_data_uploadepic, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &uploadepic::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *uploadepic::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *uploadepic::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_uploadepic))
        return static_cast<void*>(const_cast< uploadepic*>(this));
    return QDialog::qt_metacast(_clname);
}

int uploadepic::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
