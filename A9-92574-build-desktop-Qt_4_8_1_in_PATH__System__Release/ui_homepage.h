/********************************************************************************
** Form generated from reading UI file 'homepage.ui'
**
** Created: Tue May 27 11:17:46 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HOMEPAGE_H
#define UI_HOMEPAGE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_HomePage
{
public:
    QAction *actionExit;
    QLabel *label;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *profile_Button;
    QPushButton *uploadphoto_Button;
    QPushButton *test_Button;
    QPushButton *logout_Button;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout_2;
    QLabel *friend_title_label;
    QLabel *friend_label;
    QHBoxLayout *horizontalLayout;
    QPushButton *reject_Button;
    QPushButton *confirm_Button;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *pictitle_label;
    QLabel *imagedateTime_label;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *previous_Button;
    QPushButton *like_Button;
    QPushButton *next_Button;

    void setupUi(QDialog *HomePage)
    {
        if (HomePage->objectName().isEmpty())
            HomePage->setObjectName(QString::fromUtf8("HomePage"));
        HomePage->resize(1125, 703);
        actionExit = new QAction(HomePage);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("exit");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionExit->setIcon(icon);
        label = new QLabel(HomePage);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(460, 20, 161, 61));
        QFont font;
        font.setFamily(QString::fromUtf8("Purisa"));
        font.setPointSize(20);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        label->setFont(font);
        layoutWidget = new QWidget(HomePage);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(970, 20, 141, 151));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        profile_Button = new QPushButton(layoutWidget);
        profile_Button->setObjectName(QString::fromUtf8("profile_Button"));
        profile_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(profile_Button);

        uploadphoto_Button = new QPushButton(layoutWidget);
        uploadphoto_Button->setObjectName(QString::fromUtf8("uploadphoto_Button"));
        uploadphoto_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(uploadphoto_Button);

        test_Button = new QPushButton(layoutWidget);
        test_Button->setObjectName(QString::fromUtf8("test_Button"));
        test_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(test_Button);

        logout_Button = new QPushButton(layoutWidget);
        logout_Button->setObjectName(QString::fromUtf8("logout_Button"));
        logout_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(logout_Button);

        layoutWidget1 = new QWidget(HomePage);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 20, 180, 77));
        verticalLayout_2 = new QVBoxLayout(layoutWidget1);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        friend_title_label = new QLabel(layoutWidget1);
        friend_title_label->setObjectName(QString::fromUtf8("friend_title_label"));

        verticalLayout_2->addWidget(friend_title_label);

        friend_label = new QLabel(layoutWidget1);
        friend_label->setObjectName(QString::fromUtf8("friend_label"));

        verticalLayout_2->addWidget(friend_label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        reject_Button = new QPushButton(layoutWidget1);
        reject_Button->setObjectName(QString::fromUtf8("reject_Button"));

        horizontalLayout->addWidget(reject_Button);

        confirm_Button = new QPushButton(layoutWidget1);
        confirm_Button->setObjectName(QString::fromUtf8("confirm_Button"));

        horizontalLayout->addWidget(confirm_Button);


        verticalLayout_2->addLayout(horizontalLayout);

        scrollArea = new QScrollArea(HomePage);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setGeometry(QRect(160, 100, 801, 521));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 799, 519));
        scrollArea->setWidget(scrollAreaWidgetContents);
        layoutWidget2 = new QWidget(HomePage);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(280, 630, 521, 71));
        verticalLayout_3 = new QVBoxLayout(layoutWidget2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        pictitle_label = new QLabel(layoutWidget2);
        pictitle_label->setObjectName(QString::fromUtf8("pictitle_label"));

        horizontalLayout_3->addWidget(pictitle_label);

        imagedateTime_label = new QLabel(layoutWidget2);
        imagedateTime_label->setObjectName(QString::fromUtf8("imagedateTime_label"));

        horizontalLayout_3->addWidget(imagedateTime_label);


        verticalLayout_3->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        previous_Button = new QPushButton(layoutWidget2);
        previous_Button->setObjectName(QString::fromUtf8("previous_Button"));

        horizontalLayout_2->addWidget(previous_Button);

        like_Button = new QPushButton(layoutWidget2);
        like_Button->setObjectName(QString::fromUtf8("like_Button"));

        horizontalLayout_2->addWidget(like_Button);

        next_Button = new QPushButton(layoutWidget2);
        next_Button->setObjectName(QString::fromUtf8("next_Button"));

        horizontalLayout_2->addWidget(next_Button);


        verticalLayout_3->addLayout(horizontalLayout_2);

        layoutWidget->raise();
        label->raise();
        layoutWidget->raise();
        layoutWidget->raise();
        scrollArea->raise();

        retranslateUi(HomePage);

        QMetaObject::connectSlotsByName(HomePage);
    } // setupUi

    void retranslateUi(QDialog *HomePage)
    {
        HomePage->setWindowTitle(QApplication::translate("HomePage", "Dialog", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("HomePage", "exit", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionExit->setToolTip(QApplication::translate("HomePage", "exit", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("HomePage", "HomePage", 0, QApplication::UnicodeUTF8));
        profile_Button->setText(QApplication::translate("HomePage", "Profile", 0, QApplication::UnicodeUTF8));
        uploadphoto_Button->setText(QApplication::translate("HomePage", "upload photo", 0, QApplication::UnicodeUTF8));
        test_Button->setText(QApplication::translate("HomePage", "Test", 0, QApplication::UnicodeUTF8));
        logout_Button->setText(QApplication::translate("HomePage", "Logout", 0, QApplication::UnicodeUTF8));
        friend_title_label->setText(QApplication::translate("HomePage", "friend request from :", 0, QApplication::UnicodeUTF8));
        friend_label->setText(QApplication::translate("HomePage", "friend", 0, QApplication::UnicodeUTF8));
        reject_Button->setText(QApplication::translate("HomePage", "reject", 0, QApplication::UnicodeUTF8));
        confirm_Button->setText(QApplication::translate("HomePage", "confirm", 0, QApplication::UnicodeUTF8));
        pictitle_label->setText(QString());
        imagedateTime_label->setText(QString());
        previous_Button->setText(QApplication::translate("HomePage", "Previous", 0, QApplication::UnicodeUTF8));
        like_Button->setText(QApplication::translate("HomePage", "Like", 0, QApplication::UnicodeUTF8));
        next_Button->setText(QApplication::translate("HomePage", "Next", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class HomePage: public Ui_HomePage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HOMEPAGE_H
