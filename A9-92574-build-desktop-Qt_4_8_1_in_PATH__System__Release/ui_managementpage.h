/********************************************************************************
** Form generated from reading UI file 'managementpage.ui'
**
** Created: Wed May 21 23:34:51 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MANAGEMENTPAGE_H
#define UI_MANAGEMENTPAGE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ManagementPage
{
public:
    QLabel *title_label;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_5;
    QLabel *adduser_label;
    QLineEdit *adduser_lineedit;
    QPushButton *adduser_enter_Button;
    QWidget *layoutWidget_2;
    QVBoxLayout *verticalLayout_7;
    QLabel *deleteuser_label;
    QLineEdit *deleteuser_lineedit;
    QPushButton *delete_enter_Button;
    QPushButton *OK_Button;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QPushButton *adduser_Button;
    QPushButton *deleteuser_Button;

    void setupUi(QDialog *ManagementPage)
    {
        if (ManagementPage->objectName().isEmpty())
            ManagementPage->setObjectName(QString::fromUtf8("ManagementPage"));
        ManagementPage->resize(400, 184);
        title_label = new QLabel(ManagementPage);
        title_label->setObjectName(QString::fromUtf8("title_label"));
        title_label->setGeometry(QRect(50, 10, 311, 31));
        QFont font;
        font.setFamily(QString::fromUtf8("Purisa"));
        font.setPointSize(20);
        font.setBold(true);
        font.setWeight(75);
        title_label->setFont(font);
        layoutWidget = new QWidget(ManagementPage);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setEnabled(true);
        layoutWidget->setGeometry(QRect(280, 50, 111, 81));
        QFont font1;
        font1.setPointSize(8);
        layoutWidget->setFont(font1);
        verticalLayout_5 = new QVBoxLayout(layoutWidget);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        adduser_label = new QLabel(layoutWidget);
        adduser_label->setObjectName(QString::fromUtf8("adduser_label"));
        adduser_label->setEnabled(true);
        QFont font2;
        font2.setPointSize(10);
        adduser_label->setFont(font2);

        verticalLayout_5->addWidget(adduser_label);

        adduser_lineedit = new QLineEdit(layoutWidget);
        adduser_lineedit->setObjectName(QString::fromUtf8("adduser_lineedit"));
        adduser_lineedit->setEnabled(true);

        verticalLayout_5->addWidget(adduser_lineedit);

        adduser_enter_Button = new QPushButton(layoutWidget);
        adduser_enter_Button->setObjectName(QString::fromUtf8("adduser_enter_Button"));
        adduser_enter_Button->setEnabled(true);
        adduser_enter_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_5->addWidget(adduser_enter_Button);

        layoutWidget_2 = new QWidget(ManagementPage);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setEnabled(true);
        layoutWidget_2->setGeometry(QRect(10, 50, 111, 81));
        layoutWidget_2->setFont(font1);
        verticalLayout_7 = new QVBoxLayout(layoutWidget_2);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        deleteuser_label = new QLabel(layoutWidget_2);
        deleteuser_label->setObjectName(QString::fromUtf8("deleteuser_label"));
        deleteuser_label->setEnabled(true);
        deleteuser_label->setFont(font2);

        verticalLayout_7->addWidget(deleteuser_label);

        deleteuser_lineedit = new QLineEdit(layoutWidget_2);
        deleteuser_lineedit->setObjectName(QString::fromUtf8("deleteuser_lineedit"));
        deleteuser_lineedit->setEnabled(true);

        verticalLayout_7->addWidget(deleteuser_lineedit);

        delete_enter_Button = new QPushButton(layoutWidget_2);
        delete_enter_Button->setObjectName(QString::fromUtf8("delete_enter_Button"));
        delete_enter_Button->setEnabled(true);
        delete_enter_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_7->addWidget(delete_enter_Button);

        OK_Button = new QPushButton(ManagementPage);
        OK_Button->setObjectName(QString::fromUtf8("OK_Button"));
        OK_Button->setGeometry(QRect(150, 140, 101, 31));
        widget = new QWidget(ManagementPage);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(130, 50, 131, 81));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        adduser_Button = new QPushButton(widget);
        adduser_Button->setObjectName(QString::fromUtf8("adduser_Button"));

        verticalLayout->addWidget(adduser_Button);

        deleteuser_Button = new QPushButton(widget);
        deleteuser_Button->setObjectName(QString::fromUtf8("deleteuser_Button"));

        verticalLayout->addWidget(deleteuser_Button);


        retranslateUi(ManagementPage);

        QMetaObject::connectSlotsByName(ManagementPage);
    } // setupUi

    void retranslateUi(QDialog *ManagementPage)
    {
        ManagementPage->setWindowTitle(QApplication::translate("ManagementPage", "Dialog", 0, QApplication::UnicodeUTF8));
        title_label->setText(QApplication::translate("ManagementPage", "Managemenet Page", 0, QApplication::UnicodeUTF8));
        adduser_label->setText(QApplication::translate("ManagementPage", "Enter username", 0, QApplication::UnicodeUTF8));
        adduser_enter_Button->setText(QApplication::translate("ManagementPage", "Enter", 0, QApplication::UnicodeUTF8));
        deleteuser_label->setText(QApplication::translate("ManagementPage", "Enter username", 0, QApplication::UnicodeUTF8));
        delete_enter_Button->setText(QApplication::translate("ManagementPage", "Enter", 0, QApplication::UnicodeUTF8));
        OK_Button->setText(QApplication::translate("ManagementPage", "OK", 0, QApplication::UnicodeUTF8));
        adduser_Button->setText(QApplication::translate("ManagementPage", "Add User", 0, QApplication::UnicodeUTF8));
        deleteuser_Button->setText(QApplication::translate("ManagementPage", "Delete User", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ManagementPage: public Ui_ManagementPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MANAGEMENTPAGE_H
