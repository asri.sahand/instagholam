/********************************************************************************
** Form generated from reading UI file 'profilepage.ui'
**
** Created: Tue May 27 01:08:42 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROFILEPAGE_H
#define UI_PROFILEPAGE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ProfilePage
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *home_Button;
    QPushButton *uploadphoto_Button;
    QPushButton *logout_Button;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout_2;
    QLabel *username_label;
    QLabel *profile_label;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_3;
    QPushButton *addfriend_Button;
    QPushButton *unfriend_Button;
    QPushButton *changepass_Button;
    QPushButton *showfriendsButton;
    QPushButton *showusers_Button;
    QPushButton *pushButton;
    QPushButton *test_Button;
    QWidget *layoutWidget3;
    QVBoxLayout *verticalLayout_4;
    QLabel *pass_label;
    QLineEdit *newpass_lineEdit;
    QPushButton *Enter_Button;
    QWidget *layoutWidget_2;
    QVBoxLayout *verticalLayout_6;
    QLabel *unfriend_label;
    QLineEdit *unfriend_textedit;
    QPushButton *enter_unfriend_Button;
    QWidget *layoutWidget4;
    QVBoxLayout *verticalLayout_5;
    QLabel *addfriend_label;
    QLineEdit *addfriend_textedit;
    QPushButton *enter_friend_Button;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QLabel *image;
    QWidget *layoutWidget_3;
    QVBoxLayout *verticalLayout_7;
    QLabel *friends_profile_label;
    QLineEdit *friends_profile_textline;
    QPushButton *friends_profile_Enter_Button;
    QWidget *widget;
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *horizontalLayout_2;
    QLabel *pictitle_label;
    QLabel *imagedateTime_label;
    QHBoxLayout *horizontalLayout;
    QPushButton *Previous_Button;
    QPushButton *like_Button;
    QPushButton *next_Button;

    void setupUi(QDialog *ProfilePage)
    {
        if (ProfilePage->objectName().isEmpty())
            ProfilePage->setObjectName(QString::fromUtf8("ProfilePage"));
        ProfilePage->resize(1126, 717);
        layoutWidget = new QWidget(ProfilePage);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(1000, 10, 110, 128));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        home_Button = new QPushButton(layoutWidget);
        home_Button->setObjectName(QString::fromUtf8("home_Button"));
        home_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(home_Button);

        uploadphoto_Button = new QPushButton(layoutWidget);
        uploadphoto_Button->setObjectName(QString::fromUtf8("uploadphoto_Button"));
        uploadphoto_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(uploadphoto_Button);

        logout_Button = new QPushButton(layoutWidget);
        logout_Button->setObjectName(QString::fromUtf8("logout_Button"));
        logout_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(logout_Button);

        layoutWidget1 = new QWidget(ProfilePage);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(490, 10, 151, 96));
        verticalLayout_2 = new QVBoxLayout(layoutWidget1);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        username_label = new QLabel(layoutWidget1);
        username_label->setObjectName(QString::fromUtf8("username_label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Purisa"));
        font.setPointSize(20);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        username_label->setFont(font);

        verticalLayout_2->addWidget(username_label);

        profile_label = new QLabel(layoutWidget1);
        profile_label->setObjectName(QString::fromUtf8("profile_label"));
        profile_label->setFont(font);

        verticalLayout_2->addWidget(profile_label);

        layoutWidget2 = new QWidget(ProfilePage);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(10, 20, 120, 227));
        verticalLayout_3 = new QVBoxLayout(layoutWidget2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        addfriend_Button = new QPushButton(layoutWidget2);
        addfriend_Button->setObjectName(QString::fromUtf8("addfriend_Button"));
        addfriend_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_3->addWidget(addfriend_Button);

        unfriend_Button = new QPushButton(layoutWidget2);
        unfriend_Button->setObjectName(QString::fromUtf8("unfriend_Button"));
        unfriend_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_3->addWidget(unfriend_Button);

        changepass_Button = new QPushButton(layoutWidget2);
        changepass_Button->setObjectName(QString::fromUtf8("changepass_Button"));
        changepass_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_3->addWidget(changepass_Button);

        showfriendsButton = new QPushButton(layoutWidget2);
        showfriendsButton->setObjectName(QString::fromUtf8("showfriendsButton"));

        verticalLayout_3->addWidget(showfriendsButton);

        showusers_Button = new QPushButton(layoutWidget2);
        showusers_Button->setObjectName(QString::fromUtf8("showusers_Button"));

        verticalLayout_3->addWidget(showusers_Button);

        pushButton = new QPushButton(layoutWidget2);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout_3->addWidget(pushButton);

        test_Button = new QPushButton(layoutWidget2);
        test_Button->setObjectName(QString::fromUtf8("test_Button"));
        test_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_3->addWidget(test_Button);

        layoutWidget3 = new QWidget(ProfilePage);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(10, 470, 111, 81));
        verticalLayout_4 = new QVBoxLayout(layoutWidget3);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        pass_label = new QLabel(layoutWidget3);
        pass_label->setObjectName(QString::fromUtf8("pass_label"));
        QFont font1;
        font1.setPointSize(8);
        pass_label->setFont(font1);

        verticalLayout_4->addWidget(pass_label);

        newpass_lineEdit = new QLineEdit(layoutWidget3);
        newpass_lineEdit->setObjectName(QString::fromUtf8("newpass_lineEdit"));
        newpass_lineEdit->setEchoMode(QLineEdit::Password);

        verticalLayout_4->addWidget(newpass_lineEdit);

        Enter_Button = new QPushButton(layoutWidget3);
        Enter_Button->setObjectName(QString::fromUtf8("Enter_Button"));
        Enter_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_4->addWidget(Enter_Button);

        layoutWidget_2 = new QWidget(ProfilePage);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setEnabled(true);
        layoutWidget_2->setGeometry(QRect(10, 380, 112, 81));
        verticalLayout_6 = new QVBoxLayout(layoutWidget_2);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        unfriend_label = new QLabel(layoutWidget_2);
        unfriend_label->setObjectName(QString::fromUtf8("unfriend_label"));
        unfriend_label->setEnabled(true);
        unfriend_label->setFont(font1);

        verticalLayout_6->addWidget(unfriend_label);

        unfriend_textedit = new QLineEdit(layoutWidget_2);
        unfriend_textedit->setObjectName(QString::fromUtf8("unfriend_textedit"));
        unfriend_textedit->setEnabled(true);

        verticalLayout_6->addWidget(unfriend_textedit);

        enter_unfriend_Button = new QPushButton(layoutWidget_2);
        enter_unfriend_Button->setObjectName(QString::fromUtf8("enter_unfriend_Button"));
        enter_unfriend_Button->setEnabled(true);
        enter_unfriend_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_6->addWidget(enter_unfriend_Button);

        layoutWidget4 = new QWidget(ProfilePage);
        layoutWidget4->setObjectName(QString::fromUtf8("layoutWidget4"));
        layoutWidget4->setEnabled(true);
        layoutWidget4->setGeometry(QRect(10, 290, 112, 78));
        layoutWidget4->setFont(font1);
        verticalLayout_5 = new QVBoxLayout(layoutWidget4);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        addfriend_label = new QLabel(layoutWidget4);
        addfriend_label->setObjectName(QString::fromUtf8("addfriend_label"));
        addfriend_label->setEnabled(true);
        addfriend_label->setFont(font1);

        verticalLayout_5->addWidget(addfriend_label);

        addfriend_textedit = new QLineEdit(layoutWidget4);
        addfriend_textedit->setObjectName(QString::fromUtf8("addfriend_textedit"));
        addfriend_textedit->setEnabled(true);

        verticalLayout_5->addWidget(addfriend_textedit);

        enter_friend_Button = new QPushButton(layoutWidget4);
        enter_friend_Button->setObjectName(QString::fromUtf8("enter_friend_Button"));
        enter_friend_Button->setEnabled(true);
        enter_friend_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_5->addWidget(enter_friend_Button);

        scrollArea = new QScrollArea(ProfilePage);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setGeometry(QRect(150, 110, 801, 521));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 799, 519));
        image = new QLabel(scrollAreaWidgetContents);
        image->setObjectName(QString::fromUtf8("image"));
        image->setGeometry(QRect(10, 20, 171, 81));
        scrollArea->setWidget(scrollAreaWidgetContents);
        layoutWidget_3 = new QWidget(ProfilePage);
        layoutWidget_3->setObjectName(QString::fromUtf8("layoutWidget_3"));
        layoutWidget_3->setGeometry(QRect(10, 560, 112, 81));
        verticalLayout_7 = new QVBoxLayout(layoutWidget_3);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        friends_profile_label = new QLabel(layoutWidget_3);
        friends_profile_label->setObjectName(QString::fromUtf8("friends_profile_label"));
        friends_profile_label->setFont(font1);

        verticalLayout_7->addWidget(friends_profile_label);

        friends_profile_textline = new QLineEdit(layoutWidget_3);
        friends_profile_textline->setObjectName(QString::fromUtf8("friends_profile_textline"));
        friends_profile_textline->setEchoMode(QLineEdit::Normal);

        verticalLayout_7->addWidget(friends_profile_textline);

        friends_profile_Enter_Button = new QPushButton(layoutWidget_3);
        friends_profile_Enter_Button->setObjectName(QString::fromUtf8("friends_profile_Enter_Button"));
        friends_profile_Enter_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_7->addWidget(friends_profile_Enter_Button);

        widget = new QWidget(ProfilePage);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(340, 640, 461, 71));
        verticalLayout_8 = new QVBoxLayout(widget);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        pictitle_label = new QLabel(widget);
        pictitle_label->setObjectName(QString::fromUtf8("pictitle_label"));

        horizontalLayout_2->addWidget(pictitle_label);

        imagedateTime_label = new QLabel(widget);
        imagedateTime_label->setObjectName(QString::fromUtf8("imagedateTime_label"));

        horizontalLayout_2->addWidget(imagedateTime_label);


        verticalLayout_8->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Previous_Button = new QPushButton(widget);
        Previous_Button->setObjectName(QString::fromUtf8("Previous_Button"));

        horizontalLayout->addWidget(Previous_Button);

        like_Button = new QPushButton(widget);
        like_Button->setObjectName(QString::fromUtf8("like_Button"));

        horizontalLayout->addWidget(like_Button);

        next_Button = new QPushButton(widget);
        next_Button->setObjectName(QString::fromUtf8("next_Button"));

        horizontalLayout->addWidget(next_Button);


        verticalLayout_8->addLayout(horizontalLayout);


        retranslateUi(ProfilePage);

        QMetaObject::connectSlotsByName(ProfilePage);
    } // setupUi

    void retranslateUi(QDialog *ProfilePage)
    {
        ProfilePage->setWindowTitle(QApplication::translate("ProfilePage", "Dialog", 0, QApplication::UnicodeUTF8));
        home_Button->setText(QApplication::translate("ProfilePage", "Home", 0, QApplication::UnicodeUTF8));
        uploadphoto_Button->setText(QApplication::translate("ProfilePage", "upload photo", 0, QApplication::UnicodeUTF8));
        logout_Button->setText(QApplication::translate("ProfilePage", "Logout", 0, QApplication::UnicodeUTF8));
        username_label->setText(QApplication::translate("ProfilePage", "username", 0, QApplication::UnicodeUTF8));
        profile_label->setText(QApplication::translate("ProfilePage", "Profile", 0, QApplication::UnicodeUTF8));
        addfriend_Button->setText(QApplication::translate("ProfilePage", "Add friend", 0, QApplication::UnicodeUTF8));
        unfriend_Button->setText(QApplication::translate("ProfilePage", "Unfriend", 0, QApplication::UnicodeUTF8));
        changepass_Button->setText(QApplication::translate("ProfilePage", "Change pass", 0, QApplication::UnicodeUTF8));
        showfriendsButton->setText(QApplication::translate("ProfilePage", "Show friends", 0, QApplication::UnicodeUTF8));
        showusers_Button->setText(QApplication::translate("ProfilePage", "Show users", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("ProfilePage", "friends profile", 0, QApplication::UnicodeUTF8));
        test_Button->setText(QApplication::translate("ProfilePage", "show liked pics", 0, QApplication::UnicodeUTF8));
        pass_label->setText(QApplication::translate("ProfilePage", "add new password", 0, QApplication::UnicodeUTF8));
        newpass_lineEdit->setText(QString());
        Enter_Button->setText(QApplication::translate("ProfilePage", "Enter", 0, QApplication::UnicodeUTF8));
        unfriend_label->setText(QApplication::translate("ProfilePage", "Enter friend username", 0, QApplication::UnicodeUTF8));
        enter_unfriend_Button->setText(QApplication::translate("ProfilePage", "Enter", 0, QApplication::UnicodeUTF8));
        addfriend_label->setText(QApplication::translate("ProfilePage", "Enter friend username", 0, QApplication::UnicodeUTF8));
        enter_friend_Button->setText(QApplication::translate("ProfilePage", "Enter", 0, QApplication::UnicodeUTF8));
        image->setText(QString());
        friends_profile_label->setText(QApplication::translate("ProfilePage", "Enter friend username", 0, QApplication::UnicodeUTF8));
        friends_profile_textline->setText(QString());
        friends_profile_Enter_Button->setText(QApplication::translate("ProfilePage", "Enter", 0, QApplication::UnicodeUTF8));
        pictitle_label->setText(QString());
        imagedateTime_label->setText(QString());
        Previous_Button->setText(QApplication::translate("ProfilePage", "Previous", 0, QApplication::UnicodeUTF8));
        like_Button->setText(QApplication::translate("ProfilePage", "Like", 0, QApplication::UnicodeUTF8));
        next_Button->setText(QApplication::translate("ProfilePage", "Next", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ProfilePage: public Ui_ProfilePage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROFILEPAGE_H
