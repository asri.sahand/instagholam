/********************************************************************************
** Form generated from reading UI file 'show_friends.ui'
**
** Created: Sun May 18 23:49:48 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHOW_FRIENDS_H
#define UI_SHOW_FRIENDS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_show_friends
{
public:
    QPushButton *pushButton;

    void setupUi(QDialog *show_friends)
    {
        if (show_friends->objectName().isEmpty())
            show_friends->setObjectName(QString::fromUtf8("show_friends"));
        show_friends->resize(400, 300);
        pushButton = new QPushButton(show_friends);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(290, 250, 91, 31));

        retranslateUi(show_friends);

        QMetaObject::connectSlotsByName(show_friends);
    } // setupUi

    void retranslateUi(QDialog *show_friends)
    {
        show_friends->setWindowTitle(QApplication::translate("show_friends", "Dialog", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("show_friends", "OK", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class show_friends: public Ui_show_friends {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHOW_FRIENDS_H
