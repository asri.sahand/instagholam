/********************************************************************************
** Form generated from reading UI file 'showlikedpics.ui'
**
** Created: Fri May 23 15:11:41 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHOWLIKEDPICS_H
#define UI_SHOWLIKEDPICS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_showlikedpics
{
public:
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QLabel *imagedateTime_label;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QPushButton *previous_Button;
    QPushButton *next_Button;

    void setupUi(QDialog *showlikedpics)
    {
        if (showlikedpics->objectName().isEmpty())
            showlikedpics->setObjectName(QString::fromUtf8("showlikedpics"));
        showlikedpics->resize(401, 331);
        scrollArea = new QScrollArea(showlikedpics);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setGeometry(QRect(10, 10, 371, 231));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 369, 229));
        scrollArea->setWidget(scrollAreaWidgetContents);
        imagedateTime_label = new QLabel(showlikedpics);
        imagedateTime_label->setObjectName(QString::fromUtf8("imagedateTime_label"));
        imagedateTime_label->setGeometry(QRect(110, 250, 181, 21));
        widget = new QWidget(showlikedpics);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(80, 280, 231, 41));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        previous_Button = new QPushButton(widget);
        previous_Button->setObjectName(QString::fromUtf8("previous_Button"));

        horizontalLayout->addWidget(previous_Button);

        next_Button = new QPushButton(widget);
        next_Button->setObjectName(QString::fromUtf8("next_Button"));

        horizontalLayout->addWidget(next_Button);


        retranslateUi(showlikedpics);

        QMetaObject::connectSlotsByName(showlikedpics);
    } // setupUi

    void retranslateUi(QDialog *showlikedpics)
    {
        showlikedpics->setWindowTitle(QApplication::translate("showlikedpics", "Dialog", 0, QApplication::UnicodeUTF8));
        imagedateTime_label->setText(QString());
        previous_Button->setText(QApplication::translate("showlikedpics", "Previous", 0, QApplication::UnicodeUTF8));
        next_Button->setText(QApplication::translate("showlikedpics", "Next", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class showlikedpics: public Ui_showlikedpics {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHOWLIKEDPICS_H
