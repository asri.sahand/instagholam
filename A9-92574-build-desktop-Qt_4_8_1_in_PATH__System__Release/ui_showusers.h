/********************************************************************************
** Form generated from reading UI file 'showusers.ui'
**
** Created: Mon May 19 17:26:55 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHOWUSERS_H
#define UI_SHOWUSERS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_showusers
{
public:
    QPushButton *OK_Button;

    void setupUi(QDialog *showusers)
    {
        if (showusers->objectName().isEmpty())
            showusers->setObjectName(QString::fromUtf8("showusers"));
        showusers->resize(400, 300);
        OK_Button = new QPushButton(showusers);
        OK_Button->setObjectName(QString::fromUtf8("OK_Button"));
        OK_Button->setGeometry(QRect(150, 260, 98, 27));

        retranslateUi(showusers);

        QMetaObject::connectSlotsByName(showusers);
    } // setupUi

    void retranslateUi(QDialog *showusers)
    {
        showusers->setWindowTitle(QApplication::translate("showusers", "Dialog", 0, QApplication::UnicodeUTF8));
        OK_Button->setText(QApplication::translate("showusers", "OK", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class showusers: public Ui_showusers {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHOWUSERS_H
