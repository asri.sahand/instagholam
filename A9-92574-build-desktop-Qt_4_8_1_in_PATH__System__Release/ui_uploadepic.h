/********************************************************************************
** Form generated from reading UI file 'uploadepic.ui'
**
** Created: Sat May 17 19:37:02 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPLOADEPIC_H
#define UI_UPLOADEPIC_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_uploadepic
{
public:
    QPushButton *Logout_Button;

    void setupUi(QDialog *uploadepic)
    {
        if (uploadepic->objectName().isEmpty())
            uploadepic->setObjectName(QString::fromUtf8("uploadepic"));
        uploadepic->resize(400, 300);
        Logout_Button = new QPushButton(uploadepic);
        Logout_Button->setObjectName(QString::fromUtf8("Logout_Button"));
        Logout_Button->setGeometry(QRect(290, 10, 98, 27));

        retranslateUi(uploadepic);

        QMetaObject::connectSlotsByName(uploadepic);
    } // setupUi

    void retranslateUi(QDialog *uploadepic)
    {
        uploadepic->setWindowTitle(QApplication::translate("uploadepic", "Dialog", 0, QApplication::UnicodeUTF8));
        Logout_Button->setText(QApplication::translate("uploadepic", "Logout", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class uploadepic: public Ui_uploadepic {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPLOADEPIC_H
