/********************************************************************************
** Form generated from reading UI file 'uploadpic.ui'
**
** Created: Tue May 27 00:59:47 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPLOADPIC_H
#define UI_UPLOADPIC_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_uploadepic
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *home_Button;
    QPushButton *profile_Button;
    QPushButton *test_Button;
    QPushButton *Logout_Button;
    QLabel *image;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout_2;
    QPushButton *open_Button;
    QPushButton *showimage_Button;
    QWidget *widget;
    QVBoxLayout *verticalLayout_3;
    QLabel *title_label;
    QLineEdit *pictitle_lineEdit;
    QHBoxLayout *horizontalLayout;
    QPushButton *cancel_Button;
    QPushButton *OK_Button;

    void setupUi(QDialog *uploadepic)
    {
        if (uploadepic->objectName().isEmpty())
            uploadepic->setObjectName(QString::fromUtf8("uploadepic"));
        uploadepic->resize(703, 477);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(uploadepic->sizePolicy().hasHeightForWidth());
        uploadepic->setSizePolicy(sizePolicy);
        uploadepic->setMinimumSize(QSize(0, 0));
        layoutWidget = new QWidget(uploadepic);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(610, 10, 87, 128));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        home_Button = new QPushButton(layoutWidget);
        home_Button->setObjectName(QString::fromUtf8("home_Button"));
        home_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(home_Button);

        profile_Button = new QPushButton(layoutWidget);
        profile_Button->setObjectName(QString::fromUtf8("profile_Button"));
        profile_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(profile_Button);

        test_Button = new QPushButton(layoutWidget);
        test_Button->setObjectName(QString::fromUtf8("test_Button"));
        test_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(test_Button);

        Logout_Button = new QPushButton(layoutWidget);
        Logout_Button->setObjectName(QString::fromUtf8("Logout_Button"));
        Logout_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(Logout_Button);

        image = new QLabel(uploadepic);
        image->setObjectName(QString::fromUtf8("image"));
        image->setGeometry(QRect(160, 20, 421, 341));
        layoutWidget1 = new QWidget(uploadepic);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(20, 10, 102, 62));
        verticalLayout_2 = new QVBoxLayout(layoutWidget1);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        open_Button = new QPushButton(layoutWidget1);
        open_Button->setObjectName(QString::fromUtf8("open_Button"));

        verticalLayout_2->addWidget(open_Button);

        showimage_Button = new QPushButton(layoutWidget1);
        showimage_Button->setObjectName(QString::fromUtf8("showimage_Button"));

        verticalLayout_2->addWidget(showimage_Button);

        widget = new QWidget(uploadepic);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(270, 380, 189, 87));
        verticalLayout_3 = new QVBoxLayout(widget);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        title_label = new QLabel(widget);
        title_label->setObjectName(QString::fromUtf8("title_label"));

        verticalLayout_3->addWidget(title_label);

        pictitle_lineEdit = new QLineEdit(widget);
        pictitle_lineEdit->setObjectName(QString::fromUtf8("pictitle_lineEdit"));

        verticalLayout_3->addWidget(pictitle_lineEdit);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        cancel_Button = new QPushButton(widget);
        cancel_Button->setObjectName(QString::fromUtf8("cancel_Button"));

        horizontalLayout->addWidget(cancel_Button);

        OK_Button = new QPushButton(widget);
        OK_Button->setObjectName(QString::fromUtf8("OK_Button"));

        horizontalLayout->addWidget(OK_Button);


        verticalLayout_3->addLayout(horizontalLayout);


        retranslateUi(uploadepic);

        QMetaObject::connectSlotsByName(uploadepic);
    } // setupUi

    void retranslateUi(QDialog *uploadepic)
    {
        uploadepic->setWindowTitle(QApplication::translate("uploadepic", "Dialog", 0, QApplication::UnicodeUTF8));
        home_Button->setText(QApplication::translate("uploadepic", "Home", 0, QApplication::UnicodeUTF8));
        profile_Button->setText(QApplication::translate("uploadepic", "Profile", 0, QApplication::UnicodeUTF8));
        test_Button->setText(QApplication::translate("uploadepic", "Test", 0, QApplication::UnicodeUTF8));
        Logout_Button->setText(QApplication::translate("uploadepic", "Logout", 0, QApplication::UnicodeUTF8));
        image->setText(QString());
        open_Button->setText(QApplication::translate("uploadepic", "Open", 0, QApplication::UnicodeUTF8));
        showimage_Button->setText(QApplication::translate("uploadepic", "Show image", 0, QApplication::UnicodeUTF8));
        title_label->setText(QApplication::translate("uploadepic", "Enter a title for your picture", 0, QApplication::UnicodeUTF8));
        cancel_Button->setText(QApplication::translate("uploadepic", "Cancel", 0, QApplication::UnicodeUTF8));
        OK_Button->setText(QApplication::translate("uploadepic", "OK", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class uploadepic: public Ui_uploadepic {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPLOADPIC_H
