/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created: Tue May 20 23:25:27 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QTextBrowser>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLineEdit *register_Username;
    QLineEdit *register_Password;
    QPushButton *register_Button;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QWidget *layoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *login_Username;
    QLineEdit *login_Password;
    QPushButton *login_Button;
    QLabel *label_4;
    QPushButton *exit_Button;
    QLabel *title;
    QTextBrowser *textBrowser;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(571, 430);
        layoutWidget = new QWidget(Widget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(90, 140, 151, 131));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        register_Username = new QLineEdit(layoutWidget);
        register_Username->setObjectName(QString::fromUtf8("register_Username"));

        verticalLayout->addWidget(register_Username);

        register_Password = new QLineEdit(layoutWidget);
        register_Password->setObjectName(QString::fromUtf8("register_Password"));
        register_Password->setEchoMode(QLineEdit::Password);

        verticalLayout->addWidget(register_Password);

        register_Button = new QPushButton(layoutWidget);
        register_Button->setObjectName(QString::fromUtf8("register_Button"));
        register_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(register_Button);

        label = new QLabel(Widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 140, 81, 41));
        label_2 = new QLabel(Widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(20, 190, 71, 31));
        label_3 = new QLabel(Widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(290, 190, 71, 31));
        layoutWidget_2 = new QWidget(Widget);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(360, 140, 151, 131));
        verticalLayout_2 = new QVBoxLayout(layoutWidget_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        login_Username = new QLineEdit(layoutWidget_2);
        login_Username->setObjectName(QString::fromUtf8("login_Username"));

        verticalLayout_2->addWidget(login_Username);

        login_Password = new QLineEdit(layoutWidget_2);
        login_Password->setObjectName(QString::fromUtf8("login_Password"));
        login_Password->setEchoMode(QLineEdit::Password);

        verticalLayout_2->addWidget(login_Password);

        login_Button = new QPushButton(layoutWidget_2);
        login_Button->setObjectName(QString::fromUtf8("login_Button"));
        login_Button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(login_Button);

        label_4 = new QLabel(Widget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(290, 140, 81, 41));
        exit_Button = new QPushButton(Widget);
        exit_Button->setObjectName(QString::fromUtf8("exit_Button"));
        exit_Button->setGeometry(QRect(240, 360, 111, 41));
        exit_Button->setCursor(QCursor(Qt::PointingHandCursor));
        title = new QLabel(Widget);
        title->setObjectName(QString::fromUtf8("title"));
        title->setGeometry(QRect(220, 50, 151, 61));
        QFont font;
        font.setFamily(QString::fromUtf8("UnDotum"));
        font.setPointSize(20);
        font.setBold(true);
        font.setItalic(false);
        font.setUnderline(false);
        font.setWeight(75);
        font.setStrikeOut(false);
        font.setKerning(true);
        title->setFont(font);
        title->setCursor(QCursor(Qt::ArrowCursor));
        title->setMouseTracking(false);
        title->setFrameShape(QFrame::NoFrame);
        title->setFrameShadow(QFrame::Plain);
        textBrowser = new QTextBrowser(Widget);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(200, 290, 201, 51));
        textBrowser->viewport()->setProperty("cursor", QVariant(QCursor(Qt::ArrowCursor)));

        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", 0, QApplication::UnicodeUTF8));
        register_Button->setText(QApplication::translate("Widget", "register", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Widget", "username", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Widget", "password", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Widget", "password", 0, QApplication::UnicodeUTF8));
        login_Button->setText(QApplication::translate("Widget", "login", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Widget", "username", 0, QApplication::UnicodeUTF8));
        exit_Button->setText(QApplication::translate("Widget", "Exit", 0, QApplication::UnicodeUTF8));
        title->setText(QApplication::translate("Widget", "Instagholam", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
