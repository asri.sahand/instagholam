#-------------------------------------------------
#
# Project created by QtCreator 2014-05-15T23:23:05
#
#-------------------------------------------------

QT       += core gui

TARGET = A9-92574
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    User.cpp \
    Date.cpp \
    Update_info.cpp \
    homepage.cpp \
    profilepage.cpp \
    show_pages.cpp \
    uploadpic.cpp \
    show_friends.cpp \
    showusers.cpp \
    loginpage.cpp \
    managementpage.cpp \
    showlikedpics.cpp

HEADERS  += widget.h \
    User.h \
    Image.h \
    Date.h \
    Bad_exceptions.h \
    Update_info.h \
    Commands.h \
    homepage.h \
    profilepage.h \
    show_pages.h \
    uploadpic.h \
    show_friends.h \
    showusers.h \
    loginpage.h \
    managementpage.h \
    showlikedpics.h

FORMS    += widget.ui \
    homepage.ui \
    profilepage.ui \
    uploadpic.ui \
    show_friends.ui \
    showusers.ui \
    loginpage.ui \
    managementpage.ui \
    showlikedpics.ui
