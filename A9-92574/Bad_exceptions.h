/*
 * Bad_exceptions.h
 *
 *  Created on: May 15, 2014
 *      Author: sahand
 */

#ifndef BAD_EXCEPTIONS_H_
#define BAD_EXCEPTIONS_H_

#include "User.h"

class repetitive_user {};
class incorrect_user_or_pass {};
class exist_in_friends {};
class not_exist_in_friends {};
class user_not_exist {};
class wrong_order {};
class empty_username_or_password {};
class have_not_friend_request {};
class not_enter_pass {};

#endif /* BAD_EXCEPTIONS_H_ */
