/*
 * Date.cpp
 *
 *  Created on: May 16, 2014
 *      Author: sahand
 */

#include "Date.h"

#include <iostream>

using namespace std;

ostream& operator<<(ostream& out , const Date& d)
{
    out << d.year << "/" << d.month << "/" << d.day;
    return out;
}

bool Date::operator<(Date date)
{
    if(year < date.get_year()  ||
        (year == date.get_year() && month < date.get_month()) ||
        (year == date.get_year() && month == date.get_month() && day < date.get_day())) {
            return true;
    }
    else
        return false;
}
