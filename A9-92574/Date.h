/*
 * Date.h
 *
 *  Created on: May 16, 2014
 *      Author: sahand
 */

#ifndef DATE_H_
#define DATE_H_

#include <iostream>

using namespace std;

class Date {
public:
    //Date(unsigned int y , unsigned int m , unsigned int d) : year(y) , month(m) , day(d) {}

    unsigned int get_year() { return year; }
    unsigned int get_month() { return month; }
    unsigned int get_day() { return day; }

    bool operator<(Date date);
    friend ostream& operator<<(ostream& out , const Date& d);

private:
    unsigned int year;
    unsigned int month;
    unsigned int day;
};

#endif /* DATE_H_ */
