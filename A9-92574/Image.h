/*
 * Image.h
 *
 *  Created on: May 16, 2014
 *      Author: sahand
 */

#ifndef IMAGE_H_
#define IMAGE_H_

#include "Date.h"

using namespace std;

class Image {
public:
    void set_title(string t) { title = t; }
    void set_user_send(string u) { user_send = u; }
    void set_image_loc(string img_loc) { image_loc = img_loc; }
    void set_upload_dateTime_str(string dateTime) { upload_dateTime_str = dateTime; }
    void set_upload_time_str(string time) { upload_time_str = time; }

    FILE* get_file() { return file; }
    string get_image_loc() { return image_loc; }
    string get_title() { return title; }
    string get_upload_time_str() { return upload_time_str; }
    string get_upload_dateTime_str() { return upload_dateTime_str; }
    Date get_upload_date() { return upload_date; }
    string get_user_send() { return user_send; }
    vector<string> get_comments() { return comments; }
    vector<string> get_users_liked() { return users_liked; }

private:
    FILE* file;
    string image_loc;
    string title;
    string upload_time_str;
    string upload_dateTime_str;
    Date upload_date;
    string user_send;
    vector<string> comments;
    vector<string> users_liked;
};

#endif /* IMAGE_H_ */

