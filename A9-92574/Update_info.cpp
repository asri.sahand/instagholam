/*
 * Update_info.cpp
 *
 *  Created on: May 15, 2014
 *      Author: sahand
 */

#include "User.h"
#include "Update_info.h"

vector<User> get_contacts_from_file(vector<User> users)
{
    string user , pass;
    ifstream user_file;
    user_file.open("users.info");
    if(user_file != NULL) {
        while(!user_file.eof()) {
            user_file >> user >> pass;
            if(user_file.eof())
                break;
            if(user_file.eof())
                break;
            User temp(user , pass);
            users.push_back(temp);
        }
    }
    user_file.close();
    return users;
}

void update_user(vector<User>& users , User user)
{
    for(unsigned int i = 0 ; i < users.size() ; i++) {
        if(users[i].get_user_name() == user.get_user_name())
            users[i] = user;
    }
}
