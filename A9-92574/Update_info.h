/*
 * Update_info.h
 *
 *  Created on: May 15, 2014
 *      Author: sahand
 */

#ifndef UPDATE_INFO_H_
#define UPDATE_INFO_H_

#include "User.h"

vector<User> get_contacts_from_file(vector<User> users);
void update_user(vector<User>& users , User user);

#endif /* UPDATE_INFO_H_ */
