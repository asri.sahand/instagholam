/*
 * User.cpp
 *
 *  Created on: May 15, 2014
 *      Author: sahand
 */

#include "User.h"
#include "Bad_exceptions.h"
#include "Update_info.h"

#define last_pics 3

using namespace std;

User User::operator=(User user)
{
    user_name = user.get_user_name();
    password = user.get_password();
    friends = user.get_friends();
    images = user.get_images();
    images_liked = get_images_liked();
    return *this;
}

bool User::register_user(vector<User>& users , string user , string pass)
{
    if(user == "" || pass == "") {
        throw empty_username_or_password();
    }
    for(unsigned int i = 0 ; i < users.size() ; i++) {
        if(users[i].get_user_name() == user)
            throw repetitive_user();
    }
    user_name = user;
    password = pass;
    //cout << "Register successfully. :)" << endl;
    users.push_back(*this);
    users[0].add_friend_to_vec(*this);
    friends.push_back(users[0]);
    update_user(users , *this);
    //ofstream users_file("users.info" , ios::app);
    //users_file << user << " " << pass << endl;
    //users_file.close();
    return true;
}

bool User::login_user(vector<User> users , string user , string pass)
{
    for(unsigned int i = 0 ; i < users.size() ; i++) {
        if(users[i].get_user_name() == user && users[i].get_password() == pass) {
            *this = users[i];
            return true;
        }
    }
    throw incorrect_user_or_pass();
}

void User::erase_friend_from_vec(User user)
{
    for(unsigned int i = 0 ; i <friends.size() ; i++) {
        if(user.get_user_name() == friends[i].get_user_name()) {
            friends.erase(friends.begin() + i);
        }
    }
}

void User::add_friend(vector<User>& users , string user)
{
    for(unsigned int i = 0 ; i < friends.size() ; i++) {
        if(friends[i].get_user_name() == user)
            throw exist_in_friends();
    }
    for(unsigned int i = 0 ; i < users.size() ; i++) {
        if(users[i].get_user_name() == user) {
            ofstream requests_file((user + ".requests").c_str() , ios::app);
            requests_file << user_name << endl;
            requests_file.close();
            return;
        }
    }
    throw user_not_exist();
}

void User::unfriend(vector<User>& users , string username)
{
    for(unsigned int i = 0 ; i < friends.size() ; i++) {
        if(friends[i].get_user_name() == username) {
            friends.erase(friends.begin() + i);
            update_user(users , *this);
        }
    }
    for(unsigned int i = 0 ; i < users.size() ; i++) {
        if(users[i].get_user_name() == username) {
            users[i].erase_friend_from_vec(*this);
            return;
        }
    }
    throw user_not_exist();
}

vector<string> User::show_friends_request(vector<User>& users)
{
    vector<string> result;
    //string order;
    string user;
    ifstream requests_file((user_name + ".requests").c_str());
    if(requests_file != NULL) {
        while(!requests_file.eof()) {
            getline(requests_file , user);
            if(requests_file.eof())
                break;
            cout << "You have a friend request from " << user << endl;
            cout << "Type confirm or reject" << endl;
            result.push_back(user);
            /*
            cin >> order;
            if(order == "confirm") {
                cout << "You and " << user << " are now friends. :)" << endl;

                for(unsigned int i = 0 ; i < users.size() ; i++) {
                    if(user == users[i].get_user_name()) {
                        friends.push_back(users[i]);
                        users[i].add_friend_to_vec(*this);
                        break;
                    }
                }
                for(unsigned int i = 0 ; i < users.size() ; i++) {
                    if(users[i].get_user_name() == user_name)
                        users[i] = *this;
                }
            }
            else if(order == "reject") {
                cout << "Friend request rejecte." << endl;
                requests_file.close();
            }
            else {
                requests_file.close();
                throw wrong_order();
            }
            */
        }
    }
    else {
        requests_file.close();
        throw have_not_friend_request();
    }
    requests_file.close();
    remove((user_name + ".requests").c_str());
    return result;
}

void User::confirm_friend_requset(vector<User>& users , string user)
{
    cout << "You and " << user << " are now friends. :)" << endl;

    for(unsigned int i = 0 ; i < users.size() ; i++) {
        if(user == users[i].get_user_name()) {
            friends.push_back(users[i]);
            cout << user_name << endl;
            users[i].add_friend_to_vec(*this);
            cout << users[i].get_user_name() << endl;
            break;
        }
    }
    for(unsigned int i = 0 ; i < users.size() ; i++) {
        if(users[i].get_user_name() == user_name)
            users[i] = *this;
    }
}

void User::show_news(vector<User>& users , vector<string>& friends_req)
{
    friends_req = show_friends_request(users);
}

void User::change_password(vector<User>& users , string pass)
{
    if(pass == "")
        throw not_enter_pass();
    password = pass;
    update_user(users , *this);
}

void User::add_image_to_fav(vector<User>& users , Image image)
{

    for(unsigned int i = 0 ; i < images_liked.size() ; i++) {
        //if images[i] == image
        //hoseleye tarife operator== nabood :)
        if(images_liked[i].get_upload_dateTime_str() == image.get_upload_dateTime_str())
            return;
    }

    images_liked.push_back(image);
    update_user(users , *this);
}

void User::set_user_info(string u , string p , vector<User> f , vector<Image> i , vector<Image> i_l)
{
    user_name = u;
    password = p;
    friends = f;
    images = i;
    images_liked = i_l;
}

void User::logout()
{
    user_name = "";
    password = "";
    friends.clear();
    images.clear();
    images_liked.clear();
}

vector<Image> User::get_last_friends_images()
{
    vector<Image> last_friends_images;
    for(unsigned int i = 0 ; i < friends.size() ; i++) {
        if(friends[i].get_images().size() < last_pics) {
            for(unsigned int j = friends[i].get_images().size() ; j > 0 ; j--) {
                last_friends_images.push_back(friends[i].get_images()[j-1]);
            }
        }
        else {
            for(unsigned int j = friends[i].get_images().size() ; j > friends[i].get_images().size()-last_pics ; j--) {
                last_friends_images.push_back(friends[i].get_images()[j-1]);
            }
        }
    }
    return last_friends_images;
}

void User::update_friends_info(vector<User> users)
{
    for(unsigned int i = 0 ; i < users.size() ; i++) {
        for(unsigned int j = 0 ; j < friends.size() ; j++) {
            if(users[i].get_user_name() == friends[j].get_user_name())
                friends[j] = users[i];
        }
    }
}
