/*
 * User.h
 *
 *  Created on: May 15, 2014
 *      Author: sahand
 */

#ifndef USER_H_
#define USER_H_

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include "Image.h"

using namespace std;

class User {
public:
    User(string user , string pass) : user_name(user) , password(pass) {}

    void set_user_info(string u , string p , vector<User> f , vector<Image> i , vector<Image> i_l);

    string get_user_name() { return user_name; }
    string get_password() { return password; }
    vector<User> get_friends() { return friends; }
    vector<Image> get_images() { return images; }
    vector<Image> get_images_liked() { return images_liked; }

    vector<Image> get_last_friends_images();

    void set_friends(User new_friend) { friends.push_back(new_friend); }

    void erase_friend_from_vec(User user);
    void add_friend_to_vec(User user) { friends.push_back(user); }
    void add_image_to_vec(Image image) { images.push_back(image); }
    void add_image_to_fav(vector<User> &users, Image image);

    bool register_user( vector<User>& users , string user , string pass);
    bool login_user(vector<User> users , string user , string pass);
    void add_friend(vector<User>& users , string user_name);
    void unfriend(vector<User>& users , string user_name);
    void confirm_friend_requset(vector<User>& users , string user);
    void show_news(vector<User>& users , vector<string>& friends_req);
    void change_password(vector<User>& users , string pass);
    void add_photo();
    void logout();

    vector<string> show_friends_request(vector<User>& users);

    User operator=(User user);

    void update_friends_info(vector<User> users);

private:
    string user_name;
    string password;
    vector<User> friends;
    vector<Image> images;
    vector<Image> images_liked;
};


#endif /* USER_H_ */
