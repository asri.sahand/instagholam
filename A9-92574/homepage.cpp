#include "homepage.h"
#include "ui_homepage.h"

#include "profilepage.h"
#include "show_pages.h"

HomePage::HomePage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HomePage)
{
    ui->setupUi(this);
}

HomePage::~HomePage()
{
    delete ui;
}

void HomePage::on_test_Button_clicked()
{
    cout << user->get_images().size() << endl;
    for(unsigned int i = 0 ; i < user->get_images().size() ; i++) {
        cout << user->get_images()[i].get_image_loc() << endl;
    }
}

void HomePage::set_info(User* u , vector<User> v_u)
{
    for(unsigned int i = 0 ; i < v_u.size() ; i++) {
        users.push_back(v_u[i]);
    }
    user = u;
    pic_num = 0;
}

void HomePage::on_profile_Button_clicked()
{
    close();
    show_profilepage(user , users);
}

void HomePage::on_logout_Button_clicked()
{
    close();
    show_loginpage(user , users);
}

void HomePage::on_uploadphoto_Button_clicked()
{
    close();
    show_uploadpicpage(user , users);
}

void HomePage::hide_friend_request()
{
    ui->friend_label->hide();
    ui->friend_title_label->hide();
    ui->confirm_Button->hide();
    ui->reject_Button->hide();
}

void HomePage::show_news()
{
    vector<string> friends_req;
        try {
//            QString username_qstr;
//            username_qstr = QString::fromStdString(friend_req->get_user_name());
//            ui->friend_label->setText(username_qstr);
            User u("" , "");
            friends_req = user->show_friends_request(users);
            show_friend_request();
            friend_req = new User("" , "");
            for(unsigned int i = 0 ; i < users.size() ; i++) {
                if(friends_req[0] == users[i].get_user_name())
                    u = users[i];
            }
            *friend_req = u;
            QString username_qstr;
            username_qstr = QString::fromStdString(friend_req->get_user_name());
            cout << friend_req->get_user_name() << endl;
            ui->friend_label->setText(username_qstr);
        } catch(have_not_friend_request) {
            cout << "no requests" << endl;
            hide_friend_request();
        }
}

void HomePage::show_friend_request()
{
    ui->friend_label->show();
    ui->friend_title_label->show();
    ui->confirm_Button->show();
    ui->reject_Button->show();
}

void HomePage::on_confirm_Button_clicked()
{
    user->confirm_friend_requset(users , friend_req->get_user_name());
    QMessageBox msg;
    msg.setText("Now you are friends\n:-)");
    msg.exec();
    hide_friend_request();
}

void HomePage::on_reject_Button_clicked()
{
    //remove((user->get_user_name() + ".requests").c_str());
    hide_friend_request();
}

void HomePage::on_next_Button_clicked()
{
    if(pic_num < user->get_last_friends_images().size() - 1) {
        pic_num++;
    }
    cout << pic_num << endl;
    show_pics();
}

void HomePage::show_pics()
{
    if(user->get_last_friends_images().size() != 0) {
        QPixmap test(user->get_last_friends_images()[pic_num].get_image_loc().c_str());
        QLabel *new_image = new QLabel;
        new_image->setPixmap(test.scaled(800 , 600 , Qt::KeepAspectRatio));
        ui->scrollArea->setWidget(new_image);
        QString title_qstr = QString::fromStdString(user->get_last_friends_images()[pic_num].get_title());
        ui->pictitle_label->setText(title_qstr);
        QString dateTime_qstr = QString::fromStdString(user->get_last_friends_images()[pic_num].get_upload_dateTime_str());
        ui->imagedateTime_label->setText(dateTime_qstr);
        new_image->show();
    }
}

void HomePage::on_previous_Button_clicked()
{
    if(pic_num > 0)
        pic_num--;
    cout << pic_num << endl;
    show_pics();
}

void HomePage::on_like_Button_clicked()
{
    cout << "<" << user->get_images_liked().size() << ">" << endl;
    user->add_image_to_fav(users , user->get_last_friends_images()[pic_num]);
    cout << "<" << user->get_images_liked().size() << ">" << endl;
    update_user(users , *user);
}
