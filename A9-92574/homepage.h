#ifndef HOMEPAGE_H
#define HOMEPAGE_H

#include <QDialog>

#include "widget.h"

namespace Ui {
class HomePage;
}

class HomePage : public QDialog
{
    Q_OBJECT
    
public:
    explicit HomePage(QWidget *parent = 0);
    ~HomePage();
    void set_info(User* u , vector<User> v_u);
    User* get_user() { return user; }
    vector<User> get_users() { return users; }
    void show_news();
    void hide_friend_request();
    void show_friend_request();
    void show_pics();
    
private slots:
    void on_test_Button_clicked();

    void on_logout_Button_clicked();

    void on_profile_Button_clicked();

    void on_uploadphoto_Button_clicked();

    void on_confirm_Button_clicked();

    void on_reject_Button_clicked();

    void on_next_Button_clicked();

    void on_previous_Button_clicked();

    void on_like_Button_clicked();

private:
    Ui::HomePage *ui;
    User *user;
    vector<User> users;
    QMenu *menue;
    User *friend_req;
    unsigned int pic_num;
};

#endif // HOMEPAGE_H
