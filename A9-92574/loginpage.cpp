#include "loginpage.h"
#include "ui_loginpage.h"

LoginPage::LoginPage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginPage)
{
    ui->setupUi(this);
}

LoginPage::~LoginPage()
{
    delete ui;
}

void LoginPage::set_info(User* u , vector<User> v_u)
{
    for(unsigned int i = 0 ; i < v_u.size() ; i++) {
        users.push_back(v_u[i]);
    }
    user = u;
}

void LoginPage::on_exit_Button_clicked()
{
    close();
}

void LoginPage::on_register_Button_clicked()
{
    QString User_name = ui->register_Username->text();
    QString Password = ui->register_Password->text();

    string user_name = User_name.toStdString();
    string password = Password.toStdString();

    try {
        User* baghali = new User("" , "");
        baghali->register_user(users , user_name , password);
        set_user(baghali);
        baghali->login_user(users , user_name , password);
        set_user(baghali);

        close();

        update_user(users , *user);

        //show HomePage
        show_homepage(user , users);

        update_user(users , *user);

    }   catch (repetitive_user) {
        ui->textBrowser->clear();
        ui->textBrowser->append("This username is already existing :-(");
        QMessageBox msg;
        msg.setText("This username is already existing\n:-(");
        msg.exec();
    } catch (empty_username_or_password) {
        ui->textBrowser->clear();
        ui->textBrowser->append("Enter username and password");
        QMessageBox msg;
        msg.setText("Enter username and password");
        msg.exec();
    }
}

void LoginPage::set_user(User *u)
{
    user = new User("" , "");
    user->set_user_info(u->get_user_name(), u->get_password(), u->get_friends(), u->get_images(), u->get_images_liked());
}

void LoginPage::on_login_Button_clicked()
{
    QString User_name = ui->login_Username->text();
    QString Password = ui->login_Password->text();

    string user_name = User_name.toStdString();
    string password = Password.toStdString();

    try {
        User* baghali = new User("" , "");
        baghali->login_user(users , user_name , password);
        set_user(baghali);

        user->update_friends_info(users);

        close();

        if(user_name == "ghodrat") {
            show_managementpage(user , users);
        }
        else {
            update_user(users , *user);

            //show HomePage
            show_homepage(user , users);
        }

        update_user(users , *user);

    } catch (incorrect_user_or_pass) {
        ui->textBrowser->clear();
        ui->textBrowser->append("Incorrect username or password :-(");
        QMessageBox msg;
        msg.setText("Incorrect username or password\n:-(");
        msg.exec();
    }
}
