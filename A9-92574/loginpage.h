#ifndef LOGINPAGE_H
#define LOGINPAGE_H

#include <QDialog>

#include "User.h"
#include "widget.h"
#include "show_pages.h"

namespace Ui {
class LoginPage;
}

class LoginPage : public QDialog
{
    Q_OBJECT
    
public:
    explicit LoginPage(QWidget *parent = 0);
    ~LoginPage();

    void set_user(User *u);
    void set_info(User* u , vector<User> v_u);
    User* get_user() { return user; }
    vector<User> get_users() { return users; }
    
private slots:
    void on_exit_Button_clicked();

    void on_register_Button_clicked();

    void on_login_Button_clicked();

private:
    Ui::LoginPage *ui;
    User *user;
    vector<User> users;
};

#endif // LOGINPAGE_H
