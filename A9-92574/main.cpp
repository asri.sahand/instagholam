#include <QtGui/QApplication>
#include "widget.h"
#include "homepage.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    w.init_users();
    w.show();

    return a.exec();
}
