#include "managementpage.h"
#include "ui_managementpage.h"

ManagementPage::ManagementPage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ManagementPage)
{
    ui->setupUi(this);
}

ManagementPage::~ManagementPage()
{
    delete ui;
}

void ManagementPage::on_adduser_Button_clicked()
{
    ui->adduser_enter_Button->show();
    ui->adduser_label->show();
    ui->adduser_lineedit->show();
}

void ManagementPage::on_adduser_enter_Button_clicked()
{
    QString username_qstr = ui->adduser_lineedit->text();
    string username_str = username_qstr.toStdString();
    hide_adduser_label();
    try {
        add_user(username_str);
    } catch(repetitive_user) {

        QMessageBox msg;
        msg.setText("This user is exist\n:-(");
        msg.exec();
    } catch(empty_username_or_password) {
        QMessageBox msg;
        msg.setText("Enter username\n:-(");
        msg.exec();
    }

}

void ManagementPage::set_info(User* u , vector<User> v_u)
{
    for(unsigned int i = 0 ; i < v_u.size() ; i++) {
        users.push_back(v_u[i]);
    }
    user = u;
}

void ManagementPage::on_delete_enter_Button_clicked()
{
    QString username_qstr = ui->deleteuser_lineedit->text();
    string username_str = username_qstr.toStdString();
    hide_deleteuser_label();
    try {
        delete_user(username_str);
    } catch(user_not_exist) {

        QMessageBox msg;
        msg.setText("User not exist\n:-(");
        msg.exec();
    }
}

void ManagementPage::add_user(string username)
{
    if(username == "")
        throw empty_username_or_password();
    for(unsigned int i = 0; i < users.size() ; i++) {
        if(users[i].get_user_name() == username)
            throw repetitive_user();
    }
    User new_user(username , username);
    users.push_back(new_user);
}

void ManagementPage::delete_user(string username)
{
    for(unsigned int i = 0; i < users.size() ; i++) {
        if(users[i].get_user_name() == username) {
            users.erase(users.begin() + i);
            return;
        }
    }
    throw user_not_exist();
}

void ManagementPage::on_OK_Button_clicked()
{
    close();
    show_homepage(user , users);
}

void ManagementPage::hide_adduser_label()
{
    ui->adduser_enter_Button->hide();
    ui->adduser_label->hide();
    ui->adduser_lineedit->hide();
}

void ManagementPage::hide_deleteuser_label()
{
    ui->deleteuser_label->hide();
    ui->deleteuser_lineedit->hide();
    ui->delete_enter_Button->hide();
}

void ManagementPage::on_deleteuser_Button_clicked()
{
    ui->deleteuser_label->show();
    ui->deleteuser_lineedit->show();
    ui->delete_enter_Button->show();
}
