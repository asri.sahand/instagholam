#ifndef MANAGEMENTPAGE_H
#define MANAGEMENTPAGE_H

#include <QDialog>

#include "User.h"
#include "Bad_exceptions.h"
#include "widget.h"
#include "show_pages.h"

namespace Ui {
class ManagementPage;
}

class ManagementPage : public QDialog
{
    Q_OBJECT
    
public:
    explicit ManagementPage(QWidget *parent = 0);
    ~ManagementPage();

    void set_info(User* u , vector<User> v_u);
    User* get_user() { return user; }
    vector<User> get_users() { return users; }
    void add_user(string username);
    void delete_user(string username);
    void hide_adduser_label();
    void hide_deleteuser_label();
    
private slots:
    void on_adduser_Button_clicked();

    void on_adduser_enter_Button_clicked();

    void on_delete_enter_Button_clicked();

    void on_OK_Button_clicked();

    void on_deleteuser_Button_clicked();

private:
    Ui::ManagementPage *ui;
    User* user;
    vector<User> users;
};

#endif // MANAGEMENTPAGE_H
