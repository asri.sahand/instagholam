#include "profilepage.h"
#include "ui_profilepage.h"

#include "show_pages.h"

ProfilePage::ProfilePage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProfilePage)
{
    ui->setupUi(this);
}

ProfilePage::~ProfilePage()
{
    delete ui;
}

void ProfilePage::on_home_Button_clicked()
{
    close();
    show_homepage(user , users);
}

void ProfilePage::on_logout_Button_clicked()
{
    close();
    show_loginpage(user , users);
}

void ProfilePage::on_uploadphoto_Button_clicked()
{
    close();
    show_uploadpicpage(user , users);
}

void ProfilePage::on_test_Button_clicked()
{
    show_liked_images();
}

void ProfilePage::set_info(User* u , vector<User> v_u)
{
    for(unsigned int i = 0 ; i < v_u.size() ; i++) {
        users.push_back(v_u[i]);
    }
    user = u;
    pic_num = 0;
}

void ProfilePage::show_username()
{
    QString username;
    username = QString::fromStdString(user->get_user_name());
    ui->username_label->setText(username);
}

void ProfilePage::on_changepass_Button_clicked()
{
    ui->newpass_lineEdit->show();
    ui->pass_label->show();
    ui->Enter_Button->show();
}

void ProfilePage::hide_pass_label()
{
    ui->newpass_lineEdit->hide();
    ui->pass_label->hide();
    ui->Enter_Button->hide();
}

void ProfilePage::hide_addfriend_label()
{
    ui->addfriend_label->hide();
    ui->addfriend_textedit->hide();
    ui->enter_friend_Button->hide();
}

void ProfilePage::on_Enter_Button_clicked()
{
    QString newpass_qstr = ui->newpass_lineEdit->text();
    string newpass_str = newpass_qstr.toStdString();
    try {
        user->change_password(users , newpass_str);

        ui->newpass_lineEdit->hide();
        ui->pass_label->hide();
        ui->Enter_Button->hide();

        QMessageBox msg;
        msg.setText("Password changed\n:-)");
        msg.exec();
    } catch(not_enter_pass) {

        QMessageBox msg;
        msg.setText("Enter password please");
        msg.exec();
    }
}

void ProfilePage::on_addfriend_Button_clicked()
{
    ui->addfriend_textedit->show();
    ui->enter_friend_Button->show();
    ui->addfriend_label->show();
}

void ProfilePage::on_enter_friend_Button_clicked()
{
    QString username_qstr = ui->addfriend_textedit->text();
    string username_str = username_qstr.toStdString();
    try {
        user->add_friend(users , username_str);
        QMessageBox msg;
        msg.setText("Request sent\n:-)");
        msg.exec();
    } catch(user_not_exist) {
        QMessageBox msg;
        msg.setText("User not exist\n:-(");
        msg.exec();
    } catch (exist_in_friends) {
        QMessageBox msg;
        msg.setText("This user is in your friend list.");
        msg.exec();
    }

    ui->addfriend_label->hide();
    ui->addfriend_textedit->hide();
    ui->enter_friend_Button->hide();
}

void ProfilePage::hide_unfriend_label()
{
    ui->unfriend_label->hide();
    ui->unfriend_textedit->hide();
    ui->enter_unfriend_Button->hide();
}

void ProfilePage::on_unfriend_Button_clicked()
{
    ui->unfriend_label->show();
    ui->unfriend_textedit->show();
    ui->enter_unfriend_Button->show();
}

void ProfilePage::on_enter_unfriend_Button_clicked()
{
    QString username_qstr = ui->unfriend_textedit->text();
    string username_str = username_qstr.toStdString();
    try {
        user->unfriend(users , username_str);
        QMessageBox msg;
        msg.setText("The user erased from friends successfully\n:-)");
        msg.exec();
    } catch(user_not_exist) {
        QMessageBox msg;
        msg.setText("This user not exist in your friends. :-(");
        msg.exec();
    }

    ui->unfriend_label->hide();
    ui->unfriend_textedit->hide();
    ui->enter_unfriend_Button->hide();
}

void ProfilePage::on_showfriendsButton_clicked()
{
    show_showfriendspage(user , users);
}

void ProfilePage::on_showusers_Button_clicked()
{
    show_showusers(user , users);
}

void ProfilePage::show_images()
{
    if(user->get_images().size() != 0) {
        QString title_qstr;
        title_qstr = QString::fromStdString(user->get_images()[pic_num].get_title());
        ui->pictitle_label->setText(title_qstr);
        QPixmap test(user->get_images()[pic_num].get_image_loc().c_str());
        QLabel *new_image = new QLabel;
        new_image->setPixmap(test.scaled(800 , 600 , Qt::KeepAspectRatio));
        ui->scrollArea->setWidget(new_image);
        QString dateTime_qstr = QString::fromStdString(user->get_images()[pic_num].get_upload_dateTime_str());
        ui->imagedateTime_label->setText(dateTime_qstr);
        new_image->show();
        cout << user->get_images()[pic_num].get_title();
    }
}

void ProfilePage::show_liked_images()
{
    show_showlikedpage(user , users);
}

void ProfilePage::on_next_Button_clicked()
{
    if(pic_num < user->get_images().size() - 1)
        pic_num++;
    cout << pic_num << endl;
    show_images();
}

void ProfilePage::on_Previous_Button_clicked()
{
    if(pic_num > 0)
        pic_num--;
    cout << pic_num << endl;
    show_images();
}

void ProfilePage::hide_friends_profile_label()
{
    ui->friends_profile_Enter_Button->hide();
    ui->friends_profile_label->hide();
    ui->friends_profile_textline->hide();
}

void ProfilePage::on_pushButton_clicked()
{
    ui->friends_profile_Enter_Button->show();
    ui->friends_profile_label->show();
    ui->friends_profile_textline->show();
}

void ProfilePage::on_friends_profile_Enter_Button_clicked()
{
    QString username_qstr = ui->friends_profile_textline->text();
    string username_str = username_qstr.toStdString();
    show_friend_profile(username_str);
    hide_friends_profile_label();
}

void ProfilePage::hide_all_buttons()
{
    ui->addfriend_Button->hide();
    ui->changepass_Button->hide();
    ui->Enter_Button->hide();
    ui->enter_friend_Button->hide();
    ui->enter_unfriend_Button->hide();
    ui->friends_profile_Enter_Button->hide();
    ui->home_Button->hide();
    ui->logout_Button->hide();
    ui->next_Button->hide();
    ui->Previous_Button->hide();
    ui->showfriendsButton->hide();
    ui->showusers_Button->hide();
    ui->test_Button->hide();
    ui->unfriend_Button->hide();
    ui->uploadphoto_Button->hide();
    ui->pushButton->hide();
}

void ProfilePage::show_prev_next_button()
{
    ui->next_Button->show();
    ui->Previous_Button->show();
}

void ProfilePage::show_friend_profile(string username)
{
    cout << username << endl;
    for(unsigned int i = 0 ; i < user->get_friends().size() ; i++) {
        if(user->get_friends()[i].get_user_name() == username) {
            show_friends_profile(&user->get_friends()[i] , users);
            return;
        }
    }
    //throw not_exist_in_friends();
}

void ProfilePage::on_like_Button_clicked()
{
    cout << "<" << user->get_images_liked().size() << ">" << endl;
    user->add_image_to_fav(users , user->get_images()[pic_num]);
    cout << "<" << user->get_images_liked().size() << ">" << endl;
    update_user(users , *user);
}
