#ifndef PROFILEPAGE_H
#define PROFILEPAGE_H

#include <QDialog>

#include"widget.h"
#include "homepage.h"

namespace Ui {
class ProfilePage;
}

class ProfilePage : public QDialog
{
    Q_OBJECT
    
public:
    explicit ProfilePage(QWidget *parent = 0);
    ~ProfilePage();
    void set_info(User* u , vector<User> v_u);
    void show_username();

    void hide_pass_label();
    void hide_addfriend_label();
    void hide_unfriend_label();
    void show_images();
    void show_liked_images();
    void hide_friends_profile_label();
    void show_friend_profile(string user);
    void hide_all_buttons();
    void show_prev_next_button();
    
private slots:

    void on_home_Button_clicked();

    void on_logout_Button_clicked();

    void on_test_Button_clicked();

    void on_changepass_Button_clicked();

    void on_Enter_Button_clicked();

    void on_uploadphoto_Button_clicked();

    void on_addfriend_Button_clicked();

    void on_enter_friend_Button_clicked();

    void on_unfriend_Button_clicked();

    void on_enter_unfriend_Button_clicked();

    void on_showfriendsButton_clicked();

    void on_showusers_Button_clicked();

    void on_next_Button_clicked();

    void on_Previous_Button_clicked();

    void on_pushButton_clicked();

    void on_friends_profile_Enter_Button_clicked();

    void on_like_Button_clicked();

private:
    Ui::ProfilePage *ui;
    User *user;
    vector<User> users;
    unsigned int pic_num;
};

#endif // PROFILEPAGE_H
