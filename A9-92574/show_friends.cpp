#include "show_friends.h"
#include "ui_show_friends.h"

show_friends::show_friends(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::show_friends)
{
    ui->setupUi(this);
}

show_friends::~show_friends()
{
    delete ui;
}

void show_friends::set_info(User* u , vector<User> v_u)
{
    for(unsigned int i = 0 ; i < v_u.size() ; i++) {
        users.push_back(v_u[i]);
    }
    user = u;
}

void show_friends::on_pushButton_clicked()
{
    close();
}

void show_friends::showfriends()
{
    QListWidget *listWidget = new QListWidget(this);
    for(unsigned int i = 0 ; i < user->get_friends().size() ; i++) {
        QListWidgetItem *newItem = new QListWidgetItem;
        newItem->setText(user->get_friends()[i].get_user_name().c_str());
        listWidget->insertItem(i+1 , newItem);
        cout << users[i].get_user_name() << endl;
    }
}
