#ifndef SHOW_FRIENDS_H
#define SHOW_FRIENDS_H

#include <QDialog>

#include "User.h"
#include "widget.h"

namespace Ui {
class show_friends;
}

class show_friends : public QDialog
{
    Q_OBJECT
    
public:
    explicit show_friends(QWidget *parent = 0);
    ~show_friends();
    void set_info(User* u , vector<User> v_u);
    User* get_user() { return user; }
    vector<User> get_users() { return users; }
    void showfriends();
    
private slots:
    void on_pushButton_clicked();

private:
    Ui::show_friends *ui;
    User* user;
    vector<User> users;
};

#endif // SHOW_FRIENDS_H
