#include "show_pages.h"

void show_homepage(User* user , vector<User>& users)
{
    HomePage HomePage;
    HomePage.set_info(user , users);
    HomePage.show_news();
    HomePage.setModal(true);
    HomePage.exec();
}

void show_profilepage(User* user , vector<User>& users)
{
    ProfilePage ProfilePage;
    ProfilePage.set_info(user , users);
    ProfilePage.show_username();
    ProfilePage.hide_pass_label();
    ProfilePage.hide_addfriend_label();
    ProfilePage.hide_unfriend_label();
    ProfilePage.show_images();
    ProfilePage.hide_friends_profile_label();
    ProfilePage.showMaximized();
    ProfilePage.setModal(true);
    ProfilePage.exec();
}

void show_uploadpicpage(User* user , vector<User>& users)
{
    uploadepic uploadepic;
    uploadepic.set_info(user , users);
    uploadepic.hide_OK_and_cancel_Buttons();
    uploadepic.hide_title_label();
    uploadepic.setModal(true);
    uploadepic.exec();
}

void show_showfriendspage(User* user , vector<User>& users)
{
    show_friends show_friends;
    show_friends.set_info(user , users);
    show_friends.showfriends();
    show_friends.setModal(true);
    show_friends.exec();
}

void show_showusers(User* user , vector<User>& users)
{
    showusers showusers;
    showusers.set_info(user , users);
    showusers.show_users();
    showusers.setModal(true);
    showusers.exec();
}

void show_loginpage(User* user , vector<User>& users)
{
    LoginPage LoginPage;
    LoginPage.set_info(user , users);
    LoginPage.setModal(true);
    LoginPage.exec();
}

void show_friends_profile(User* user , vector<User> users)
{
    ProfilePage ProfilePage;
    ProfilePage.set_info(user , users);
    ProfilePage.hide_all_buttons();
    ProfilePage.show_username();
    ProfilePage.hide_pass_label();
    ProfilePage.hide_addfriend_label();
    ProfilePage.hide_unfriend_label();
    ProfilePage.show_images();
    ProfilePage.hide_friends_profile_label();
    ProfilePage.show_prev_next_button();
    ProfilePage.showMaximized();
    ProfilePage.setModal(true);
    ProfilePage.exec();
}

void show_managementpage(User *user, vector<User> users)
{
    ManagementPage ManagementPage;
    ManagementPage.set_info(user , users);
    ManagementPage.hide_adduser_label();
    ManagementPage.hide_deleteuser_label();
    ManagementPage.setModal(true);
    ManagementPage.exec();
}

void show_showlikedpage(User *user, vector<User> users)
{
    showlikedpics showlikedpics;
    showlikedpics.set_info(user , users);
    showlikedpics.setModal(true);
    showlikedpics.exec();
}
