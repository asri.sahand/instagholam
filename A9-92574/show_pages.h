#ifndef SHOW_PAGES_H
#define SHOW_PAGES_H

#include "widget.h"
#include "homepage.h"
#include "profilepage.h"
#include "uploadpic.h"
#include "show_friends.h"
#include "showusers.h"
#include "loginpage.h"
#include "managementpage.h"
#include "showlikedpics.h"

void show_homepage(User* user , vector<User>& users);

void show_profilepage(User* user , vector<User>& users);

void show_uploadpicpage(User* user , vector<User>& users);

void show_showfriendspage(User* user , vector<User>& users);

void show_showusers(User* user , vector<User>& users);

void show_loginpage(User* user , vector<User>& users);

void show_friends_profile(User* user , vector<User> users);

void show_managementpage(User* user , vector<User> users);

void show_showlikedpage(User* user , vector<User> users);

#endif // SHOW_PAGES_H
