#include "showlikedpics.h"
#include "ui_showlikedpics.h"

showlikedpics::showlikedpics(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::showlikedpics)
{
    ui->setupUi(this);
}

showlikedpics::~showlikedpics()
{
    delete ui;
}

void showlikedpics::set_info(User* u , vector<User> v_u)
{
    for(unsigned int i = 0 ; i < v_u.size() ; i++) {
        users.push_back(v_u[i]);
    }
    user = u;
    pic_num = 0;
}

void showlikedpics::show_pics()
{
    if(user->get_images_liked().size() != 0) {
        QPixmap test(user->get_images_liked()[pic_num].get_image_loc().c_str());
        QLabel *new_image = new QLabel;
        new_image->setPixmap(test.scaled(400 , 300 , Qt::KeepAspectRatio));
        ui->scrollArea->setWidget(new_image);
        QString dateTime_qstr = QString::fromStdString(user->get_images()[pic_num].get_upload_dateTime_str());
        ui->imagedateTime_label->setText(dateTime_qstr);
        new_image->show();
    }
}

void showlikedpics::on_next_Button_clicked()
{
    if(user->get_images_liked().size() != 0) {
        if(pic_num < user->get_images_liked().size() - 1)
            pic_num++;
        cout << pic_num << endl;
        show_pics();
    }
}

void showlikedpics::on_previous_Button_clicked()
{
    if(pic_num > 0)
        pic_num--;
    cout << pic_num << endl;
    show_pics();
}
