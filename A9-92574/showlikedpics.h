#ifndef SHOWLIKEDPICS_H
#define SHOWLIKEDPICS_H

#include <QDialog>

#include "User.h"
#include "widget.h"

namespace Ui {
class showlikedpics;
}

class showlikedpics : public QDialog
{
    Q_OBJECT
    
public:
    explicit showlikedpics(QWidget *parent = 0);
    ~showlikedpics();

    void set_info(User* u , vector<User> v_u);
    void show_pics();
    
private slots:
    void on_next_Button_clicked();

    void on_previous_Button_clicked();

private:
    Ui::showlikedpics *ui;
    User *user;
    vector<User> users;
    unsigned int pic_num;
};

#endif // SHOWLIKEDPICS_H
