#include "showusers.h"
#include "ui_showusers.h"

showusers::showusers(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::showusers)
{
    ui->setupUi(this);
}

showusers::~showusers()
{
    delete ui;
}

void showusers::set_info(User* u , vector<User> v_u)
{
    for(unsigned int i = 0 ; i < v_u.size() ; i++) {
        users.push_back(v_u[i]);
    }
    user = u;
}

void showusers::show_users()
{
    QListWidget *listWidget = new QListWidget(this);
    for(unsigned int i = 0 ; i < users.size() ; i++) {
        QListWidgetItem *newItem = new QListWidgetItem;
        newItem->setText(users[i].get_user_name().c_str());
        listWidget->insertItem(i+1 , newItem);
        cout << users[i].get_user_name() << endl;
    }
}

void showusers::on_OK_Button_clicked()
{
    close();
}
