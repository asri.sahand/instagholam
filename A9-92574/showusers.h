#ifndef SHOWUSERS_H
#define SHOWUSERS_H

#include <QDialog>

#include "User.h"
#include "widget.h"

namespace Ui {
class showusers;
}

class showusers : public QDialog
{
    Q_OBJECT
    
public:
    explicit showusers(QWidget *parent = 0);
    ~showusers();
    void set_info(User* u , vector<User> v_u);
    User* get_user() { return user; }
    vector<User> get_users() { return users; }
    void show_users();
    
private slots:
    void on_OK_Button_clicked();

private:
    Ui::showusers *ui;
    User* user;
    vector<User> users;
};

#endif // SHOWUSERS_H
