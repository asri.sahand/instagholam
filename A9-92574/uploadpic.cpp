#include "uploadpic.h"
#include "ui_uploadpic.h"

uploadepic::uploadepic(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::uploadepic)
{
    ui->setupUi(this);
}

uploadepic::~uploadepic()
{
    delete ui;
}

void uploadepic::set_info(User* u , vector<User> v_u)
{
    for(unsigned int i = 0 ; i < v_u.size() ; i++) {
        users.push_back(v_u[i]);
    }
    user = u;
}

void uploadepic::on_Logout_Button_clicked()
{
    close();
    show_loginpage(user , users);
}

void uploadepic::on_test_Button_clicked()
{
    cout << user->get_friends()[1].get_images().size() << endl;
}

void uploadepic::on_home_Button_clicked()
{
    close();
    show_homepage(user , users);
}

void uploadepic::on_profile_Button_clicked()
{
    close();
    show_profilepage(user , users);
}

void uploadepic::on_showimage_Button_clicked()
{
    QPixmap test("test.png");
    ui->image->setPixmap(test);
    ui->image->setWindowFlags(Qt::Window);
    ui->image->adjustSize();
    //ui->image->setPixmap(test.scaled(500,500 , Qt::KeepAspectRatio));
    ui->image->show();
}

void uploadepic::on_open_Button_clicked()
{
    QString filename_qstr = QFileDialog::getOpenFileName(this , tr("Open File") , "" , tr("Image Files (*.png *.jpg)"));
    string filename_str;
    filename_str = filename_qstr.toStdString();
    showing_image.set_image_loc(filename_str);
    QPixmap test(filename_str.c_str());
    ui->image->setPixmap(test);
    //ui->image->setWindowFlags(Qt::Window);
    //ui->image->adjustSize();
    ui->image->setScaledContents(true);
    //ui->image->setPixmap(test.scaled(500,500 , Qt::KeepAspectRatio));
    ui->image->show();
    ui->OK_Button->show();
    ui->title_label->show();
    ui->pictitle_lineEdit->show();
    ui->cancel_Button->show();
}

void uploadepic::hide_OK_and_cancel_Buttons()
{
    ui->cancel_Button->hide();
    ui->OK_Button->hide();
}

void uploadepic::on_cancel_Button_clicked()
{
    showing_image.set_image_loc("");
    ui->image->hide();
    hide_OK_and_cancel_Buttons();
    hide_title_label();
}

void uploadepic::on_OK_Button_clicked()
{
    ui->image->hide();
    hide_OK_and_cancel_Buttons();
    hide_title_label();
    //get current time
    QTime time = QTime::currentTime();
    QString time_qstr = time.toString();
    string time_str = time_qstr.toStdString();
    cout << time_str << endl;
    showing_image.set_upload_time_str(time_str);

    //get current date and time
    QDateTime dateTime = QDateTime::currentDateTime();
    QString dateTime_qstr = dateTime.toString();
    string dataTime_str = dateTime_qstr.toStdString();
    cout << dataTime_str << endl;
    showing_image.set_upload_dateTime_str(dataTime_str);

    QString title_qstr = ui->pictitle_lineEdit->text();
    string title_str = title_qstr.toStdString();

    showing_image.set_title(title_str);
    user->add_image_to_vec(showing_image);

    update_user(users , *user);
}

void uploadepic::hide_title_label()
{
    ui->title_label->hide();
    ui->pictitle_lineEdit->hide();
}
