#ifndef UPLOADEPIC_H
#define UPLOADEPIC_H

#include <QDialog>

#include "User.h"
#include "show_pages.h"
#include "Update_info.h"

namespace Ui {
class uploadepic;
}

class uploadepic : public QDialog
{
    Q_OBJECT
    
public:
    explicit uploadepic(QWidget *parent = 0);
    ~uploadepic();

    void set_info(User* u , vector<User> v_u);
    void hide_OK_and_cancel_Buttons();
    void hide_title_label();

private slots:
    void on_Logout_Button_clicked();

    void on_test_Button_clicked();

    void on_home_Button_clicked();

    void on_profile_Button_clicked();

    void on_showimage_Button_clicked();

    void on_open_Button_clicked();

    void on_cancel_Button_clicked();

    void on_OK_Button_clicked();

private:
    Ui::uploadepic *ui;
    User *user;
    vector<User> users;
    Image showing_image;
};

#endif // UPLOADEPIC_H
