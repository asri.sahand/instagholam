#include "widget.h"
#include "ui_widget.h"

#include "homepage.h"
#include "profilepage.h"
#include "show_pages.h"

#include <iostream>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_register_Button_clicked()
{
    QString User_name = ui->register_Username->text();
    QString Password = ui->register_Password->text();

    string user_name = User_name.toStdString();
    string password = Password.toStdString();

    try {
        User* baghali = new User("" , "");
        baghali->register_user(users , user_name , password);
        set_user(baghali);
        baghali->login_user(users , user_name , password);
        set_user(baghali);

        close();

        //show HomePage
        show_homepage(user , users);

        //show FirstPage
        //show();

//        update_user(users , *user);

    }   catch (repetitive_user) {
        ui->textBrowser->clear();
        ui->textBrowser->append("This username is already existing :-(");
        QMessageBox msg;
        msg.setText("This username is already existing\n:-(");
        msg.exec();

    } catch (empty_username_or_password) {
        ui->textBrowser->clear();
        ui->textBrowser->append("Enter username and password");
        QMessageBox msg;
        msg.setText("Enter username and password");
        msg.exec();
    }
}
void Widget::init_users()
{
    User ghodrat("ghodrat" , "ghodrat");
    users.push_back(ghodrat);
}

void Widget::on_login_Button_clicked()
{
    QString User_name = ui->login_Username->text();
    QString Password = ui->login_Password->text();

    string user_name = User_name.toStdString();
    string password = Password.toStdString();

    try {
        User* baghali = new User("" , "");
        baghali->login_user(users , user_name , password);
        set_user(baghali);

        user->update_friends_info(users);

        close();

        if(user_name == "ghodrat") {
            show_managementpage(user , users);
        }
        else {
            //show HomePage
            show_homepage(user , users);
        }

//        update_user(users , *user);

    } catch (incorrect_user_or_pass) {
        ui->textBrowser->clear();
        ui->textBrowser->append("Incorrect username or password :-(");
        QMessageBox msg;
        msg.setText("Incorrect username or password\n:-(");
        msg.exec();
    }
}

void Widget::set_user(User *u)
{
    user = new User("" , "");
    user->set_user_info(u->get_user_name(), u->get_password(), u->get_friends(), u->get_images(), u->get_images_liked());
}

void Widget::on_exit_Button_clicked()
{
    qApp->exit();
}

void Widget::set_info(User* u , vector<User> v_u)
{
    for(unsigned int i = 0 ; i < v_u.size() ; i++) {
        users.push_back(v_u[i]);
    }
    user = u;
}

void Widget::update_user(vector<User>& users , User user)
{
    for(unsigned int i = 0 ; i < users.size() ; i++) {
        if(users[i].get_user_name() == user.get_user_name())
            users[i] = user;
    }
}
