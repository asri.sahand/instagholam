#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTextEdit>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QMenu>
#include <QMenuBar>
#include <QListView>
#include <QListWidgetItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QLabel>
#include <QImage>
#include <QFileDialog>
#include <QScrollArea>
#include <QScrollBar>
#include <QTime>

#include "User.h"
#include "Bad_exceptions.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
    
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void init_users();
    void set_user(User* u);
    void set_info(User* u , vector<User> v_u);
    void update_user(vector<User>& users , User user);

    User* get_user() { return user; }
    vector<User> get_users() { return users; }
    
private slots:
    void on_register_Button_clicked();

    void on_login_Button_clicked();

    void on_exit_Button_clicked();

private:
    Ui::Widget *ui;
    User *user;
    vector<User> users;
};

#endif // WIDGET_H
